Welcome to Uploader Bot!
==================

Util for upload resized picture to a remote storage (Google Drive)

 There are three main step

 1. Set resize queue for local picture (./bot schedule path_local_folder_with_picture).
    Local folder have to include only picture with format png and jpg
 2. Resize picture from queue and move file to the RESIZE_FOLDER
 3. Upload to the google drive


Requirements
------------
 - php pdo driver
 - mysql db
 - ImageMagic
 - Composer


Instalation
-----------
1. Create config.php
2. Install composer require "composer install"
3. Create mysql table for queue use create.sql
4. Set access for execution "sudo chmod +x bot"
5. add alias to ~/.bashrc for example alias bot='/usr/bin/php /var/www/uploader_bot/bot.php' and reboot system

Google Drive
------------

1.  To get secret file for Server To Server intagration constants in config DRIVE_SECRET_FILE and DRIVE_CLIENT_ID

MySQL
-----

1.  Table for queue located into constant DB_FILE_QUEUE_TABLE


Commands
--------

- bot schedule source_folder_path
- bot resize -n <count>
- bot upload -n <count>
- bot status
- bot retry -n <count>


STATUS
------

- resize - file to ready resize
- upload - file resized and to ready upload to remote storage
- done   - file upload to the remote storage
- failed -  during upload had problems, but files may upload late by "./bot retry" command