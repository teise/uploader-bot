#!/usr/bin/php
<?php

/**
 * Script resize images jpg and png and upload them
 * to the remove source (Google Drive)
 *
 */

require __DIR__ . "/config.php";
require __DIR__ . '/vendor/autoload.php';

# mysql connection
\lib\ConnectionPool::setConnectionString($dbUser, $dbPassword, $dbName);

$controller = new \lib\Controller();
$context = $controller->getContext();
$context->addParam('resizeFolder', RESIZE_FOLDER);

if (isset($argv[2]) && $argv[2] == "-n" && $argv[3] > 0) {
    $context->addParam('limit', $argv[3]);
}

if (isset($argv[1])) {

    $context->addParam('action', $argv[1]);

    if (isset($argv[2]) && $argv[2] == "-n" && $argv[3] > 0) {
        $context->addParam('limit', $argv[3]);
    } elseif (isset($argv[2])) {
        $context->addParam('source', $argv[2]);
    }
}

$controller->process();


