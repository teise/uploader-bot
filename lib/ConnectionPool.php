<?php

namespace lib;

/**
 * Class ConnectionPool
 * @package lib
 */

class ConnectionPool
{
    /**
     * @var self
     */
    private static $instance = null;

    private static $dsn = null;
    private static $username = null;
    private static $passwd = null;

    /**
     * @static
     * @return self
     */
    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @var PDO
     */
    protected $pdo = null;

    private function __clone()
    {
    }

    private function __construct()
    {
    }

    /**
     * @static
     *
     * @param string $user
     * @param string $passwd
     * @param string $dbName
     * @param string $host
     * @param string $driver
     * @param string $charset
     */
    public static function setConnectionString(
        $user,
        $passwd,
        $dbName,
        $host = "localhost",
        $driver = "mysql",
        $charset = "utf8"
    ) {
        self::$passwd = $passwd;
        self::$username = $user;
        self::$dsn = $driver . ":dbname=" . $dbName . ";host=" . $host . ";charset=" . $charset;
    }

    /**
     * @throws \PDOException
     *
     * @return void
     */
    protected function initPdo()
    {
        if (!self::$dsn) {
            throw new \PDOException("Empty connection string");
        }

        $this->pdo = $this->pdoConnection();
    }

    /**
     * @return PDO
     */
    protected function pdoConnection()
    {
        return new \PDO(self::$dsn, self::$username, self::$passwd, []);
    }

    /**
     * @return PDO
     */
    public function getPdo()
    {
        if (!$this->pdo) {
            $this->initPdo();
        }

        return $this->pdo;
    }


} 