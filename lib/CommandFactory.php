<?php


namespace lib;

use lib\Exceptions\CommandNotExistExaption;
use lib\Exceptions\CommandNotFoundException;

/**
 * Class CommandFactory
 * @package lib
 */
class CommandFactory
{

    /**
     *  command class path
     * @var string
     */
    private static $nameSpace = "\\lib\\Commands\\";

    /**
     * existing commands
     *
     *  schedule - create queue from user path with pictures
     *  resize   - resize picture and add them to the queue for upload
     *  upload   - upload picture to the remote source
     *  retry    - whether upload picture failed retry
     *  default  - command helper
     *  status   - picture status
     *
     * @var array
     */
    private static $arCommands = [
        "schedule",
        "resize",
        "upload",
        "retry",
        "default",
        "status"
    ];

    /**
     * select class
     *
     * @param null $action
     * @return mixed
     * @throws Exceptions\CommandNotFoundException
     * @throws Exceptions\CommandNotExistExaption
     */
    public static function getCommand($action = null)
    {
        if (!in_array($action, self::$arCommands)) {
            throw new CommandNotExistExaption("The command " . $action . " is not exists");
        }

        $class = self::$nameSpace . ucfirst(strtolower($action)) . "Command";

        if (!class_exists($class)) {
            throw new CommandNotFoundException("Class " . $class . " not found");
        }

        $cmd = new $class();

        return $cmd;
    }
}
