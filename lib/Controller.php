<?php

namespace lib;

use lib\Commands\CommandContext;

/**
 * Class Controller
 * @package lib
 */
class Controller
{
    private $context;

    public function __construct()
    {
        $this->context = new CommandContext();
    }

    public function getContext()
    {
        return $this->context;
    }

    /**
     *  get action and deligate to the CommandFactory
     *
     */
    public function process()
    {
        $action = $this->context->get('action');
        $action = (is_null($action)) ? "default" : $action;

        $cmd = CommandFactory::getCommand($action);

        if (!$cmd->execute($this->context)) {
            print "\n" . $this->context->getError() . "\n";
        } else {
            print "\n" . $this->context->getMessage() . "\n";
        }
    }
} 