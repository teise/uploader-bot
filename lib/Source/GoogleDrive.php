<?php

namespace lib\Source;

/**
 * google drive wrapper
 *
 * Class GoogleDrive
 * @package lib\Source
 */
class GoogleDrive extends RemoteSource
{

    protected $client = null;
    protected $service;
    protected $arPictureData;


    /**
     * get authorize
     *
     * @return \Google_Client
     */
    protected function getCredentials()
    {

        $client_email = DRIVE_CLIENT_ID;
        $private_key = file_get_contents(DRIVE_SECRET_FILE);

        $scopes = array('https://www.googleapis.com/auth/drive.file');
        $credentials = new \Google_Auth_AssertionCredentials(
            $client_email,
            $scopes,
            $private_key
        );

        $this->client = new \Google_Client();
        $this->client->setAssertionCredentials($credentials);
        if ($this->client->getAuth()->isAccessTokenExpired()) {
            $this->client->getAuth()->refreshTokenWithAssertion();
        }

        return $this->client;
    }

    /**
     * get drive service
     *
     * @return \Google_Service_Drive
     */
    protected function getService()
    {
        if (is_null($this->client)) {
            $this->getCredentials();
        }

        $this->service = new \Google_Service_Drive($this->client);

        return $this->service;
    }

    /**
     * @param null $fileName
     * @param null $type
     * @param null $filePath
     * @return null
     */
    public function uploadFiles($fileName = null, $type = null, $filePath = null)
    {
        if (is_null($fileName) || is_null($type) || is_null($filePath)) {
            return null;
        }

        $this->service = $this->getService();

        //Insert a file
        $file = new \Google_Service_Drive_DriveFile();
        $file->setTitle($fileName);
        $file->setDescription('A test document');
        $file->setMimeType($type);

        $data = file_get_contents($filePath);

        $createdFile = $this->service->files->insert(
            $file,
            array(
                'data' => $data,
                'mimeType' => $type,
                'uploadType' => 'multipart'
            )
        );

        return $createdFile->id;

    }
} 