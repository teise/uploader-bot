<?php

namespace lib\Source;


abstract class RemoteSource
{

    abstract function uploadFiles($fileName, $fileType, $filePath);

} 