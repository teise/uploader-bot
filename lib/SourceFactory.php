<?php

namespace lib;


use lib\Exceptions\SourceFactoryException;

/**
 * Class SourceFactory
 * @package lib
 */
class SourceFactory
{

    private $className = null;

    public function __construct()
    {

        if (!defined('REMOTE_SOURCE')) {
            throw new SourceFactoryException("Remote source is not set");
        }

        $this->className = 'lib\Source\\' . REMOTE_SOURCE;
    }

    /**
     * @return object
     */
    public function getRemoteSource()
    {
        return new $this->className();
    }
}

