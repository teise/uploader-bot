<?php

namespace lib\Commands;

/**
 * resize picture by Image Magick to the WIDTH and HEIGHT constants
 *
 * Class ResizeCommand
 * @package lib\Commands
 */
class ResizeCommand extends Command
{

    const WIDTH = RESIZE_WIDTH;
    const HEIGHT = RESIZE_HEIGHT;

    protected $resizedFolder = null;
    protected $arPictureForResize = null;
    protected $backgroundColor = "White";

    /**
     * @param CommandContext $context
     * @return bool
     */
    public function execute(CommandContext $context)
    {
        $this->context = $context;

        $this->resizedFolder = $this->context->get('resizeFolder');
        $limit = $this->context->get('limit');

        if (is_null($this->resizedFolder)) {
            $this->context->setError("Resize folder is not set");

            return false;
        }

        $this->arPictureForResize = $this->getQueue($this->arStatus[0], $limit);

        if (is_array($this->arPictureForResize) && count($this->arPictureForResize) > 0) {

            if (!file_exists($this->resizedFolder)) {
                $this->context->setError("Folder for resize " . $this->resizedFolder . " is not exists");
                return false;
            }

            foreach ($this->arPictureForResize as $fileId => $picturePath) {

                $sourcePath = $picturePath['FILE_PATH'] . $picturePath['FILE_NAME'];

                if ($this->imagick($sourcePath, $picturePath['FILE_NAME'])) {
                    $this->updateQueue($fileId, $this->arStatus[1]);
                    $this->deleteSourceFile($sourcePath);
                }
            }

            if (!$this->context->getError()) {
                $this->context->setMessage("Files resized and ready to upload");
            }

            return true;

        } else {

            $this->context->setError("There are not picture for resize");

            return false;
        }
    }

    /**
     * @param $sourcePath
     * @param null $fileName
     * @return bool
     */
    protected function imagick($sourcePath, $fileName = null)
    {
        if ($this->verifyImagick($sourcePath, $fileName)) {


            $image = new \Imagick($sourcePath);

            $image->scaleImage(self::WIDTH, self::HEIGHT, true);
            $image->setImageBackgroundColor($this->backgroundColor);
            $w = $image->getImageWidth();
            $h = $image->getImageHeight();
            $image->extentImage(self::WIDTH, self::HEIGHT, ($w - self::WIDTH) / 2, ($h - self::HEIGHT) / 2);
            $image->writeImage($this->resizedFolder . "/" . $fileName);
            $image->destroy();

            return true;
        }
    }

    /**
     * @param $sourcePath
     * @param null $fileName
     * @return bool
     */
    protected function  verifyImagick($sourcePath, $fileName = null)
    {

        if (!file_exists($sourcePath)) {
            $this->context->setError("Resize the path" . $sourcePath . "is not exists");
            return false;
        }

        if (is_null($fileName)) {
            $this->context->setError("Resize file name is missed");
            return false;
        }

        if (!defined('self::WIDTH') || !defined('self::HEIGHT')) {
            $this->context->setError("WIDTH or HEIGHT resize constant is not set");
            return false;
        }

        if (!class_exists("\\Imagick")) {
            $this->context->setError("Imagick class is not exists");
            return false;
        }

        return true;
    }

    /**
     * @param null $path
     * @return null
     */
    protected function deleteSourceFile($path = null)
    {
        if (is_null($path)) {
            return null;
        }

        unlink($path);
    }
} 