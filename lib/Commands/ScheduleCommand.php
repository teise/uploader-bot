<?php

namespace lib\Commands;

/**
 * class  add files to the queue from user local sourceFolder
 *
 * Class ScheduleCommand
 * @package lib\Commands
 */
class ScheduleCommand extends Command
{

    protected $sourceFolder = null;
    protected $tableName = null;

    protected $status = 'resize';

    /**
     * @param CommandContext $context
     * @return bool
     */
    public function execute(CommandContext $context)
    {
        $this->context = $context;

        $sourceFolder = $this->context->get('source');

        if (is_null($sourceFolder)) {
            $this->context->setError("Source folder have to be inserted");
            return false;
        }

        $this->prepareSourcePath($sourceFolder);

        if (!file_exists($this->sourceFolder)) {
            $this->context->setError("Source folder is not exis");
            return false;
        }

        $this->getPictureList();

        if (!$this->setResizeQueue()) {

            $this->context->setError("Can not insert file to resize queue ");
            return false;
        }

        $this->context->setMessage("Picture from " . $this->sourceFolder . " have inserted to the queue");

        return true;
    }

    /**
     * @param $sourceFolder
     * @return null|string
     */
    protected function prepareSourcePath($sourceFolder)
    {
        if (is_null($sourceFolder)) {
            return null;
        }

        if ($sourceFolder[0] === "/") {
            $this->sourceFolder = $sourceFolder;
        } else {
            $this->sourceFolder = realpath(dirname(__FILE__) . "/../..") . DIRECTORY_SEPARATOR . $sourceFolder;
        }

        if (substr($this->sourceFolder, -1) !== "/") {
            $this->sourceFolder .= "/";
        }

        return $this->sourceFolder;
    }

    /**
     * @return null
     */
    protected function getPictureList()
    {
        if (is_null($this->sourceFolder)) {
            return null;
        }

        $arFiles = array_slice(scandir($this->sourceFolder), 2);

        foreach ($arFiles as $fileName) {
            $info = new \SplFileInfo($fileName);
            $extention = $info->getExtension();

            $arFileData['FILE_NAME'] = $fileName;
            $arFileData['FILE_TYPE'] = $extention;

            $this->arPictureData[] = $arFileData;
        }

        return $this->arPictureData;
    }

    /**
     * @return bool|null
     * @throws \PDOException
     */
    protected function setResizeQueue()
    {
        if (is_null($this->arPictureData) || is_null($this->sourceFolder) || is_null($this->status)) {
            return null;
        }

        try {

            $sql = "INSERT IGNORE INTO "
                . self::TABLE_NAME . "(`STATUS`, `FILE_PATH`, FILE_NAME, FILE_TYPE) VALUES ";

            $arSqlValues = [];

            foreach ($this->arPictureData as $fileData) {

                $arSqlValues[] = "'" . $this->status . "', '" . $this->sourceFolder . "','"
                    . $fileData['FILE_NAME'] . "', 'image/" . $fileData['FILE_TYPE'] . "'";
            }

            $sql .= "(" . implode("), (", $arSqlValues) . ")";

            $this->pdo->exec($sql);

            return true;

        } catch (\PDOException $e) {
            throw new \PDOException("Error  : " . $e->getMessage());
        }
    }

} 