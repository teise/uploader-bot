<?php

namespace lib\Commands;

use lib\SourceFactory;


/**
 * common class for commands
 *
 * Class Command
 * @package lib\Commands
 *
 */
abstract class Command
{
    /**
     * db table name for queue
     */
    const TABLE_NAME = DB_FILE_QUEUE_TABLE;

    protected $arPictureData = null;
    protected $pdo = null;
    protected $context = null;
    protected $limit = 1000;
    protected $arStatus = ['resize', 'upload', 'done', 'failed'];
    protected $arPicture = null;

    function __construct()
    {
        $this->pdo = \lib\ConnectionPool::getInstance()->getPdo();
    }

    abstract function execute(CommandContext $context);

    /**
     * get picture's queue by status and limit
     *
     * @param null $status
     * @param int $limit
     * @return array|null
     */
    protected function getQueue($status = null, $limit = null)
    {
        if (!in_array($status, $this->arStatus)) {
            return null;
        }

        $limit = $limit > 0 ? $limit : $this->limit;

        $sql = 'SELECT ID, FILE_PATH, FILE_NAME, FILE_TYPE
                    FROM ' . self::TABLE_NAME . ' WHERE STATUS =:status LIMIT :limit';

        $sth = $this->pdo->prepare($sql);

        $sth->bindValue(':status', $status, \PDO::PARAM_STR);
        $sth->bindValue(':limit', $limit, \PDO::PARAM_INT);
        $sth->execute();
//        print_r($sth->errorInfo());

        $rows = $sth->fetchAll(\PDO::FETCH_ASSOC);

        $this->arPicture = [];

        foreach ($rows as $row) {
            $this->arPicture[$row['ID']] = $row;
        }

        return $this->arPicture;
    }

    /**
     * update picture queue
     *
     * @param null $fileId
     * @param null $status
     * @return null
     */
    protected function updateQueue($fileId = null, $status = null)
    {
        if (!in_array($status, $this->arStatus) || $fileId <= 0) {
            return null;
        }

        $sql = 'UPDATE ' . self::TABLE_NAME . ' SET `STATUS` = :status WHERE ID = :id';

        $sth = $this->pdo->prepare($sql);
        $sth->bindValue(':status', $status, \PDO::PARAM_STR);
        $sth->bindValue(':id', $fileId, \PDO::PARAM_INT);
        return $sth->execute();

    }

    /**
     * upload picture to remote source
     *
     * @param array $arPictureForUpload
     * @return bool|null
     */
    protected function upload($arPictureForUpload = [])
    {
        $fileId = false;

        if (count($arPictureForUpload) > 0) {

            $sf = new SourceFactory();
            $source = $sf->getRemoteSource();

            $filePath = realpath(
                    dirname(__FILE__) . "/../.."
                ) . DIRECTORY_SEPARATOR . RESIZE_FOLDER . DIRECTORY_SEPARATOR;

            foreach ($arPictureForUpload as $fileId => $fileData) {

                // set failed status
//                $this->updateQueue($fileId, $this->arStatus[3]);

                $filePath .= $fileData['FILE_NAME'];

                if (!file_exists($filePath)) {
                    continue;
                }

                $fileId = $source->uploadFiles($fileData['FILE_NAME'], $fileData['FILE_TYPE'], $filePath);

                if ($fileId) {
                    // set done status
                    $this->updateQueue($fileId, $this->arStatus[2]);
                }
            }
        }

        return $fileId;
    }
} 