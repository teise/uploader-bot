<?php

namespace lib\Commands;

/**
 * Class DefaultCommand
 * @package lib\Commands
 */
class DefaultCommand extends Command
{

    public function execute(CommandContext $context)
    {
        $message = "Uploader Bot\n" .
            "Usage:\n" .
            "  command [arguments]\n" .
            "Available commands: \n" .
            "  schedule  - Add filenames to resize queue \n" .
            "  resize  -n <count> - Resize next images from the queue \n" .
            "  status  - Output current status in format %queue%:%number_of_images% \n" .
            "  upload  -n <count> - Upload next images to remote storage \n";


        $context->setMessage($message);

        return true;
    }
} 