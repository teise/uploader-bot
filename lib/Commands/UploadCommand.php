<?php

namespace lib\Commands;

/**
 * upload files to the remote source
 *
 * Class UploadCommand
 * @package lib\Commands
 */
class UploadCommand extends Command
{

    protected $arPictureForUpload = null;
    protected $status = 'upload';

    /**
     * @param CommandContext $context
     * @return bool
     */
    public function execute(CommandContext $context)
    {
        $this->context = $context;

        $limit = $this->context->get('limit');

        $this->arPictureForUpload = $this->getQueue($this->status, $limit);

        if (!$this->upload($this->arPictureForUpload)) {
            $this->context->setError("There are not any files for upload");
            return false;
        }

        $this->context->setMessage("Files upload to the google drive");

        return true;
    }
} 