<?php

namespace lib\Commands;

/**
 * class try to upload files after they were failed
 *
 * Class RetryCommand
 * @package lib\Commands
 */
class RetryCommand extends Command
{

    protected $status = 'failed';
    protected $arPictureForUpload = null;

    /**
     * @param CommandContext $context
     * @return bool
     */
    public function execute(CommandContext $context)
    {
        $this->context = $context;

        $limit = $this->context->get('limit');

        $this->arPictureForUpload = $this->getQueue($this->status, $limit);

        if (count($this->arPictureForUpload) <= 0) {
            $this->context->setError("There are not any files to retry upload");
            return false;
        }

        if (!$this->upload($this->arPictureForUpload)) {
            $this->context->setError("No one file was uploaded");
            return false;
        }

        $this->context->setMessage("Files upload");
        return true;
    }
} 