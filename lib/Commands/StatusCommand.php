<?php

namespace lib\Commands;

/**
 * status class show information how many files resized, upload or add to the queue
 *
 * Class StatusCommand
 * @package lib\Commands
 */
class StatusCommand extends Command
{

    /**
     * @param CommandContext $context
     * @return bool
     */
    public function execute(CommandContext $context)
    {
        $this->context = $context;
        $strStatus = "";

        foreach ($this->arStatus as $status) {
            $strStatus .= "  " . $status . " " . count($this->getQueue($status)) . "\n";
        }

        if (!$strStatus) {
            $this->context->setError("Somethins wrong with to get status queue");
            return false;
        }

        $this->context->setMessage(
            "Images Processor Bot\n"
            . "\nQueue Count\n\n"
            . $strStatus
        );

        return true;
    }

} 