<?php

namespace lib\Commands;

/**
 * work with params context
 *
 * Class CommandContext
 * @package lib\Commands
 */
class CommandContext
{

    private $params = [];

    private $error = null;
    private $message = null;

    public function addParam($key, $val)
    {
        $this->params[$key] = $val;
    }

    public function get($key)
    {
        if (isset($this->params[$key])) {
            return $this->params[$key];
        }

        return null;
    }

    public function setError($error)
    {
        $this->error = $error;
    }

    public function getError()
    {
        return $this->error;
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }

    public function getMessage()
    {
        return $this->message;
    }
} 